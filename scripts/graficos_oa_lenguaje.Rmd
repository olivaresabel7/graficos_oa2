---
title: "Reporte porcentaje de logro por OA"
author: "Departamento de evaluación"
output:
  html_document:
    number_sections: yes
    toc: yes
  word_document:
    toc: yes
editor_options:
  chunk_output_type: inline
---
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = F, warning = F, message = F)
```

```{r}
library(readr)
library(readxl)
library(stringr)
library(flextable)
library(tidyr)
library(janitor)
library(dplyr)
library(tidyverse)
library(polycor)
library(ggplot2)
library(scales)
```

```{r}
### cargar base de dato para trabajar gráficos
oa_priorizado = read_excel("data/oa_criticos_santillana.xlsx", sheet = "priorizado")
oa_cobertura = read_excel("data/oa_criticos_santillana.xlsx", sheet = "cobertura")
asignatura = "Lenguaje y comunicación"




### trabajo oa priorizado
# renombrar variables de interes
oa_priorizado=oa_priorizado %>%
  rename(nivel= Nivel,
         n_oa="N° OA",
         n_estudiantes="Veces contestado",
         incorrectas = Incorrectas,
         correctas = Correctas,
         omitidas = Omitidas,
         evaluacion="Evaluación",
         area = "Área"
         ) %>%
select(nivel,
       evaluacion,
       n_oa,
       n_estudiantes,
       incorrectas,
       correctas,
       omitidas,
       area) %>% 
  mutate(n_estudiantes = as.numeric(n_estudiantes))



# trasnformación de variables en evaluación tipo diagnóstic
#extraer número del nivel para procesamietno

oa_priorizado=oa_priorizado %>%
mutate(nivel_2= str_extract(nivel, "\\d"),
       nivel_media_basica=case_when(str_extract(nivel, "\\ásico") == "ásico"~"basico",
                                    str_extract(nivel, "\\Medio")== "Medio"~"media",
                                    TRUE ~ "revisar"),
       evaluacion = case_when(evaluacion == "Diagnóstico esencial" ~ "diagnostico_esencial",
                              evaluacion == "Cierre priorizado" ~ "cierre_priorizado",
                              evaluacion == "Proceso priorizado" ~ "proceso_priorizado"),
       #cambiar los  niveles en las pruebas diagósticos
       nivel_2 = as.numeric(nivel_2),
       nivel_final = case_when(nivel_media_basica == "media" & evaluacion == "diagnostico_esencial" & nivel_2 == 1 ~ 8,
                               nivel_media_basica == "media" & evaluacion == "diagnostico_esencial" ~ nivel_2 - 1,
                               nivel_media_basica == "basico" & evaluacion == "diagnostico_esencial" ~ nivel_2 - 1,
                               TRUE ~ nivel_2
                               ),
                               nivel_media_basica = if_else(nivel_final== 8,"basico",nivel_media_basica))








#Elaboración gráfico de logro por oa por nivel para pruebas priorizadas
#niveles
niveles = oa_priorizado %>% select(nivel_final) %>% unique()
niveles = niveles$nivel_final


```

# Gráficos OA `r asignatura` para pruebas de Diagnóstico Esencial
<div style="text-align: justify"> En primer lugar, se presentan los gráficos relacionados con los diagnósticos esenciales, en cuanto a la asignación de oa´s se puede decir que se le asignó al curso anteriro el OA de las pruebas del curso que venía, es decir, el oa diagnóstico de `r asignatura` 8° básico se le asignó al nivel de 7° básico y así con todos.</div>

## Gráficos OA `r asignatura` para enseñanza básica

<div style="text-align: justify">A continuación se presentan los porcentaje de logro por OA para la asignatura de `r asignatura` en los cursos de enseñanza básica.</div>\

```{r,echo = F, aling = "center"}
#Elaboración gráfico de logro por oa por nivel para pruebas priorizadas
#niveles
niveles = oa_priorizado %>% select(nivel_final) %>%arrange(nivel_final) %>% unique()
niveles = niveles$nivel_final

grafico_basica=map(niveles,
~oa_priorizado %>%
  filter(nivel_final == .x & evaluacion == "diagnostico_esencial" & area == asignatura & nivel_media_basica =="basico") %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            porcentaje = sum(correctas, na.rm = T)/n_total ) %>%
  arrange(n_oa) %>%
  mutate(seq=seq(1:length(n_oa)),
         n_oa =paste0("O",n_oa)) %>%
  ggplot(aes(fill = n_oa, x = fct_reorder(n_oa, seq)    , y = porcentaje)) +
  geom_bar(position = "Stack", stat="identity", width = 0.6,fill="orange")+
  scale_y_continuous(labels = percent, limits = c(0,1), expand = c(0,0),breaks = seq(0, 1, by = 0.1))+
  geom_hline(aes(yintercept = mean(porcentaje),color="red") )+
  ggtitle(paste0("Porcentaje (%) cumplimiento OA"," ",.x,"° básico")) +
  xlab ("") +
  ylab(" ")+
  theme_classic() +
  theme(plot.title = element_text(hjust = 0.5,vjust = 0.5, face = "bold"),legend.position = "none")+
  scale_size(range = 1)+
  geom_text(aes(y = porcentaje, label = (round(porcentaje,2))*100),
            position = position_dodge(width = 0.5), size=1.8, vjust=-0.8, hjust=0.5 ,col="black"))


```

<div style="text-align: justify"> Para poder realizar una revisión más detallada de la cantidad de estudiantes por oa y sus porcentajes de logro, a continuación se presenta una tabla interactiva de resumen para revisión de cada oa en los niveles de enseñanza básica</div>


```{r fig.align="center"}


####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "diagnostico_esencial" & area == asignatura & nivel_media_basica =="basico" & nivel_final == 1) %>%
  group_by(n_oa) %>%
  summarise(total = n(),n_estudiantes = sum(n_estudiantes, na.rm = T), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```


```{r,echo = F, aling = "center"}
grafico_basica[[1]]
```

```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "diagnostico_esencial" & area == asignatura & nivel_media_basica =="basico" & nivel_final == 2) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```

```{r,echo = F, aling = "center"}
grafico_basica[[2]]
```

```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "diagnostico_esencial" & area == asignatura & nivel_media_basica =="basico" & nivel_final == 3) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```



```{r,echo = F, aling = "center"}
grafico_basica[[3]]
```

```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "diagnostico_esencial" & area == asignatura & nivel_media_basica =="basico" & nivel_final == 4) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```

```{r,echo = F, aling = "center"}
grafico_basica[[4]]
```

```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "diagnostico_esencial" & area == asignatura & nivel_media_basica =="basico" & nivel_final == 5) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```

```{r,echo = F, aling = "center"}
grafico_basica[[5]]
```

```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "diagnostico_esencial" & area == asignatura & nivel_media_basica =="basico" & nivel_final == 6) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```

```{r,echo = F, aling = "center"}
grafico_basica[[6]]
```

```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "diagnostico_esencial" & area == asignatura & nivel_media_basica =="basico" & nivel_final == 7) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```

```{r,echo = F, aling = "center"}
grafico_basica[[7]]
```

```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "diagnostico_esencial" & area == asignatura & nivel_media_basica =="basico" & nivel_final == 8) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```

```{r,echo = F, aling = "center"}
grafico_basica[[8]]
```



## Gráficos OA `r asignatura` para enseñanza media

<div style="text-align: justify">A continuación se presentan los porcentaje de logro por OA para la asignatura de `r asignatura` en los cursos de enseñanza media.</div>\

```{r,echo = F, aling = "center"}
niveles = oa_priorizado %>% select(nivel_media_basica, nivel_final) %>% filter(nivel_media_basica == "media") %>%
 arrange(nivel_final) %>% unique()
niveles = niveles$nivel_final

grafico_media=map(niveles,
~oa_priorizado %>%
  filter(nivel_final == .x & evaluacion == "diagnostico_esencial" & area == asignatura & nivel_media_basica =="media") %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            porcentaje = sum(correctas, na.rm = T)/n_total ) %>%
  arrange(n_oa) %>%
  mutate(seq=seq(1:length(n_oa)),
         n_oa =paste0("O",n_oa)) %>%
  ggplot(aes(fill = n_oa, x = fct_reorder(n_oa, seq)    , y = porcentaje)) +
  geom_bar(position = "Stack", stat="identity", width = 0.6,fill="orange")+
  scale_y_continuous(labels = percent, limits = c(0,1), expand = c(0,0),breaks = seq(0, 1, by = 0.1))+
  geom_hline(aes(yintercept = mean(porcentaje),color="red") )+
  ggtitle(paste0("Porcentaje (%) cumplimiento OA"," ",.x,"° medio")) +
  xlab ("") +
  ylab(" ")+
  theme_classic() +
  theme(plot.title = element_text(hjust = 0.5,vjust = 0.5, face = "bold"),legend.position = "none")+
  scale_size(range = 1)+
  geom_text(aes(y = porcentaje, label = (round(porcentaje,2))*100),
            position = position_dodge(width = 0.5), size=1.8, vjust=-0.8, hjust=0.5 ,col="black"))




```


<div style="text-align: justify"> Para poder realizar una revisión más detallada de la cantidad de estudiantes por oa y sus porcentajes de logro, a continuación se presenta una tabla interactiva de resumen para revisión de cada oa en los niveles de enseñanza media</div>

```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "diagnostico_esencial" & area == asignatura & nivel_media_basica =="media" & nivel_final == 1) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```

```{r,echo = F, aling = "center"}
grafico_media[[1]]
```


```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "diagnostico_esencial" & area == asignatura & nivel_media_basica =="media" & nivel_final == 2) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```


```{r,echo = F, aling = "center"}
grafico_media[[2]]

```


# Gráficos OA `r asignatura` para pruebas de Proceso priorizado
<div style="text-align: justify"> A continuación, se presenta la información para el proceso priorizado</div>

## Gráficos OA `r asignatura` para enseñanza básica

<div style="text-align: justify">A continuación se presentan los porcentaje de logro por OA para la asignatura de `r asignatura` en los cursos de enseñanza básica.</div>\

```{r,echo = F, aling = "center"}
#Elaboración gráfico de logro por oa por nivel para pruebas priorizadas
#niveles
niveles = oa_priorizado %>% select(nivel_final, evaluacion) %>% filter(evaluacion == "proceso_priorizado") %>%arrange(nivel_final) %>% unique()
niveles = niveles$nivel_final

grafico_basica=map(niveles,
~oa_priorizado %>%
  filter(nivel_final == .x & evaluacion == "proceso_priorizado" & area == asignatura & nivel_media_basica =="basico") %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            porcentaje = sum(correctas, na.rm = T)/n_total ) %>%
  arrange(n_oa) %>%
  mutate(seq=seq(1:length(n_oa)),
         n_oa =paste0("O",n_oa)) %>%
  ggplot(aes(fill = n_oa, x = fct_reorder(n_oa, seq)    , y = porcentaje)) +
  geom_bar(position = "Stack", stat="identity", width = 0.6,fill="orange")+
  scale_y_continuous(labels = percent, limits = c(0,1), expand = c(0,0),breaks = seq(0, 1, by = 0.1))+
  geom_hline(aes(yintercept = mean(porcentaje),color="red") )+
  ggtitle(paste0("Porcentaje (%) cumplimiento OA"," ",.x,"° básico")) +
  xlab ("") +
  ylab(" ")+
  theme_classic() +
  theme(plot.title = element_text(hjust = 0.5,vjust = 0.5, face = "bold"),legend.position = "none")+
  scale_size(range = 1)+
  geom_text(aes(y = porcentaje, label = (round(porcentaje,2))*100),
            position = position_dodge(width = 0.5), size=1.8, vjust=-0.8, hjust=0.5 ,col="black"))


```

<div style="text-align: justify"> Para poder realizar una revisión más detallada de la cantidad de estudiantes por oa y sus porcentajes de logro, a continuación se presenta una tabla interactiva de resumen para revisión de cada oa en los niveles de enseñanza básica</div>




```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "proceso_priorizado" & area == asignatura & nivel_media_basica =="basico" & nivel_final == 1) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```


```{r,echo = F, aling = "center"}
grafico_basica[[1]]
```

```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "proceso_priorizado" & area == asignatura & nivel_media_basica =="basico" & nivel_final == 2) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```

```{r,echo = F, aling = "center"}
grafico_basica[[2]]
```

```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "proceso_priorizado" & area == asignatura & nivel_media_basica =="basico" & nivel_final == 3) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```



```{r,echo = F, aling = "center"}
grafico_basica[[3]]
```

```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "proceso_priorizado" & area == asignatura & nivel_media_basica =="basico" & nivel_final == 4) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```

```{r,echo = F, aling = "center"}
grafico_basica[[4]]
```

```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "proceso_priorizado" & area == asignatura & nivel_media_basica =="basico" & nivel_final == 5) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```

```{r,echo = F, aling = "center"}
grafico_basica[[5]]
```

```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "proceso_priorizado" & area == asignatura & nivel_media_basica =="basico" & nivel_final == 6) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```

```{r,echo = F, aling = "center"}
grafico_basica[[6]]
```

```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "proceso_priorizado" & area == asignatura & nivel_media_basica =="basico" & nivel_final == 7) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```

```{r,echo = F, aling = "center"}
grafico_basica[[7]]
```

```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "proceso_priorizado" & area == asignatura & nivel_media_basica =="basico" & nivel_final == 8) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```

```{r,echo = F, aling = "center"}
grafico_basica[[8]]
```


## Gráficos OA `r asignatura` para enseñanza media

<div style="text-align: justify">A continuación se presentan los porcentaje de logro por OA para la asignatura de `r asignatura` en los cursos de enseñanza media.</div> \

```{r,echo = F, aling = "center"}
niveles = oa_priorizado %>% select(nivel_media_basica, nivel_final,evaluacion) %>% filter(nivel_media_basica == "media") %>%
  filter(evaluacion == "proceso_priorizado") %>% 
 arrange(nivel_final) %>% unique()

niveles = niveles$nivel_final

grafico_media=map(niveles,
~oa_priorizado %>%
  filter(nivel_final == .x & evaluacion == "proceso_priorizado" & area == asignatura & nivel_media_basica =="media") %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            porcentaje = sum(correctas, na.rm = T)/n_total ) %>%
  arrange(n_oa) %>%
  mutate(seq=seq(1:length(n_oa)),
         n_oa =paste0("O",n_oa)) %>%
  ggplot(aes(fill = n_oa, x = fct_reorder(n_oa, seq)    , y = porcentaje)) +
  geom_bar(position = "Stack", stat="identity", width = 0.6,fill="orange")+
  scale_y_continuous(labels = percent, limits = c(0,1), expand = c(0,0),breaks = seq(0, 1, by = 0.1))+
  geom_hline(aes(yintercept = mean(porcentaje),color="red") )+
  ggtitle(paste0("Porcentaje (%) cumplimiento OA"," ",.x,"° medio")) +
  xlab ("") +
  ylab(" ")+
  theme_classic() +
  theme(plot.title = element_text(hjust = 0.5,vjust = 0.5, face = "bold"),legend.position = "none")+
  scale_size(range = 1)+
  geom_text(aes(y = porcentaje, label = (round(porcentaje,2))*100),
            position = position_dodge(width = 0.5), size=1.8, vjust=-0.8, hjust=0.5 ,col="black"))




```


<div style="text-align: justify"> Para poder realizar una revisión más detallada de la cantidad de estudiantes por oa y sus porcentajes de logro, a continuación se presenta una tabla interactiva de resumen para revisión de cada oa en los niveles de enseñanza media</div>


```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "proceso_priorizado" & area == asignatura & nivel_media_basica =="media" & nivel_final == 1) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```

```{r,echo = F, aling = "center"}
grafico_media[[1]]
```


```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "proceso_priorizado" & area == asignatura & nivel_media_basica =="media" & nivel_final == 2) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```


```{r,echo = F, aling = "center"}
grafico_media[[2]]

```



# Gráficos OA `r asignatura` para pruebas de Cierre priorizado
<div style="text-align: justify"> En primer lugar, se presentan los gráficos relacionados el cierre priorizado.</div>

## Gráficos OA `r asignatura` para enseñanza básica

<div style="text-align: justify">A continuación se presentan los porcentaje de logro por OA para la asignatura de `r asignatura` en los cursos de enseñanza básica.</div>\

```{r,echo = F, aling = "center"}
#Elaboración gráfico de logro por oa por nivel para pruebas priorizadas
#niveles
niveles = oa_priorizado %>% select(nivel_final,evaluacion) %>% filter(evaluacion == "cierre_priorizado") %>%arrange(nivel_final) %>% unique()
niveles = niveles$nivel_final

grafico_basica=map(niveles,
~oa_priorizado %>%
  filter(nivel_final == .x & evaluacion == "cierre_priorizado" & area == asignatura  & nivel_media_basica =="basico") %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            porcentaje = sum(correctas, na.rm = T)/n_total ) %>%
  arrange(n_oa) %>%
  mutate(seq=seq(1:length(n_oa)),
         n_oa =paste0("O",n_oa)) %>%
  ggplot(aes(fill = n_oa, x = fct_reorder(n_oa, seq)    , y = porcentaje)) +
  geom_bar(position = "Stack", stat="identity", width = 0.6,fill="orange")+
  scale_y_continuous(labels = percent, limits = c(0,1), expand = c(0,0),breaks = seq(0, 1, by = 0.1))+
  geom_hline(aes(yintercept = mean(porcentaje),color="red") )+
  ggtitle(paste0("Porcentaje (%) cumplimiento OA"," ",.x,"° básico")) +
  xlab ("") +
  ylab(" ")+
  theme_classic() +
  theme(plot.title = element_text(hjust = 0.5,vjust = 0.5, face = "bold"),legend.position = "none")+
  scale_size(range = 1)+
  geom_text(aes(y = porcentaje, label = (round(porcentaje,2))*100),
            position = position_dodge(width = 0.5), size=1.8, vjust=-0.8, hjust=0.5 ,col="black"))


```

<div style="text-align: justify"> Para poder realizar una revisión más detallada de la cantidad de estudiantes por oa y sus porcentajes de logro, a continuación se presenta una tabla interactiva de resumen para revisión de cada oa en los niveles de enseñanza básica</div>


```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "cierre_priorizado" & area == asignatura & nivel_media_basica =="basico" & nivel_final == 1) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```


```{r,echo = F, aling = "center"}
grafico_basica[[1]]
```

```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "cierre_priorizado" & area == asignatura & nivel_media_basica =="basico" & nivel_final == 2) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```

```{r,echo = F, aling = "center"}
grafico_basica[[2]]
```

```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "cierre_priorizado" & area == asignatura & nivel_media_basica =="basico" & nivel_final == 3) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```



```{r,echo = F, aling = "center"}
grafico_basica[[3]]
```

```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "cierre_priorizado" & area == asignatura & nivel_media_basica =="basico" & nivel_final == 4) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```

```{r,echo = F, aling = "center"}
grafico_basica[[4]]
```

```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "cierre_priorizado" & area == asignatura & nivel_media_basica =="basico" & nivel_final == 5) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```

```{r,echo = F, aling = "center"}
grafico_basica[[5]]
```

```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "cierre_priorizado" & area == asignatura & nivel_media_basica =="basico" & nivel_final == 6) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```

```{r,echo = F, aling = "center"}
grafico_basica[[6]]
```

```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "cierre_priorizado" & area == asignatura & nivel_media_basica =="basico" & nivel_final == 7) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```

```{r,echo = F, aling = "center"}
grafico_basica[[7]]
```

```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "cierre_priorizado" & area == asignatura & nivel_media_basica =="basico" & nivel_final == 8) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```

```{r,echo = F, aling = "center"}
grafico_basica[[8]]
```



## Gráficos OA `r asignatura` para enseñanza media

<div style="text-align: justify">A continuación se presentan los porcentaje de logro por OA para la asignatura de `r asignatura` en los cursos de enseñanza media.</div>\

```{r,echo = F, aling = "center"}
niveles = oa_priorizado %>% select(nivel_media_basica, nivel_final,evaluacion) %>%filter(evaluacion == "cierre_priorizado") %>%  filter(nivel_media_basica == "media") %>%
 arrange(nivel_final) %>% unique()
niveles = niveles$nivel_final

grafico_media=map(niveles,
~oa_priorizado %>%
  filter(nivel_final == .x & evaluacion == "cierre_priorizado" & area == asignatura & nivel_media_basica =="media") %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            porcentaje = sum(correctas, na.rm = T)/n_total ) %>%
  arrange(n_oa) %>%
  mutate(seq=seq(1:length(n_oa)),
         n_oa =paste0("O",n_oa)) %>%
  ggplot(aes(fill = n_oa, x = fct_reorder(n_oa, seq)    , y = porcentaje)) +
  geom_bar(position = "Stack", stat="identity", width = 0.6,fill="orange")+
  scale_y_continuous(labels = percent, limits = c(0,1), expand = c(0,0),breaks = seq(0, 1, by = 0.1))+
  geom_hline(aes(yintercept = mean(porcentaje),color="red") )+
  ggtitle(paste0("Porcentaje (%) cumplimiento OA"," ",.x,"° medio")) +
  xlab ("") +
  ylab(" ")+
  theme_classic() +
  theme(plot.title = element_text(hjust = 0.5,vjust = 0.5, face = "bold"),legend.position = "none")+
  scale_size(range = 1)+
  geom_text(aes(y = porcentaje, label = (round(porcentaje,2))*100),
            position = position_dodge(width = 0.5), size=1.8, vjust=-0.8, hjust=0.5 ,col="black"))




```

<div style="text-align: justify"> Para poder realizar una revisión más detallada de la cantidad de estudiantes por oa y sus porcentajes de logro, a continuación se presenta una tabla interactiva de resumen para revisión de cada oa en los niveles de enseñanza media</div>

```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "cierre_priorizado" & area == asignatura & nivel_media_basica =="media" & nivel_final == 1) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```

```{r,echo = F, aling = "center"}
grafico_media[[1]]
```


```{r fig.align="center"}
####Parámetros del módelo
oa_priorizado %>%
  filter(evaluacion == "cierre_priorizado" & area == asignatura & nivel_media_basica =="media" & nivel_final == 2) %>%
  group_by(n_oa) %>%
  summarise(total = n(), n_total = sum(incorrectas, correctas, omitidas, na.rm = T),
            n_estudiantes = sum(n_estudiantes, na.rm = T),
            Correctas = sum(correctas, na.rm = T),
            Incorrectas = sum(incorrectas, na.rm = T),
            Omitidas = sum(omitidas, na.rm = T),
            "Porcentaje de logro OA (%)" =  percent(sum(Correctas, na.rm = T)/n_total,suffix = "%",decimal.mark = ".") ) %>%
  arrange(n_oa) %>% 
  DT::datatable()

```


```{r,echo = F, aling = "center"}
grafico_media[[2]]

```
